# Steam Hams but it's a graphic adventure video game
[![CI status](https://gitlab.com/tslocum/steamed-hams-but-its-a-graphic-adventure-video-game/badges/master/pipeline.svg)](https://gitlab.com/tslocum/steamed-hams-but-its-a-graphic-adventure-video-game/commits/master)
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

WIP

### Play

```bash
go run gitlab.com/tslocum/steamed-hams-but-its-a-graphic-adventure-video-game
```

## Support

Please share issues and suggestions [here](https://gitlab.com/tslocum/steamed-hams-but-its-a-graphic-adventure-video-game/issues).
****