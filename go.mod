module gitlab.com/tslocum/steamed-hams-but-its-a-graphic-adventure-video-game

go 1.14

require (
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20200420212212-258d9bec320e // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/hajimehoshi/ebiten v1.11.1
	golang.org/x/exp v0.0.0-20200513190911-00229845015e // indirect
	golang.org/x/image v0.0.0-20200430140353-33d19683fad8
	golang.org/x/mobile v0.0.0-20200329125638-4c31acba0007 // indirect
	golang.org/x/sys v0.0.0-20200519105757-fe76b779f299 // indirect
)
