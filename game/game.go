package game

import (
	"fmt"
	"image/color"
	"io/ioutil"
	"log"

	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"github.com/hajimehoshi/ebiten/inpututil"
	"github.com/hajimehoshi/ebiten/text"
)

var (
	imgChalmers   *ebiten.Image
	imgSkinner    *ebiten.Image
	imgDiningRoom *ebiten.Image

	imgSkinnerW float64

	screenW, screenH float64

	fontStylized     font.Face
	fontStylizedSize = 48

	fontStylizedLarge     font.Face
	fontStylizedLargeSize = 72

	testText = "I hope youre prepared for an unforgettable luncheon!"
)

const dpi = 72 // TODO

const shadowDistance = 3

// TODO Remove absolute path
func Initialize() {
	// Load fonts
	b, err := ioutil.ReadFile("/home/trevor/go/src/gitlab.com/tslocum/steamed-hams-but-its-a-graphic-adventure-video-game/assets/font/simpson.ttf")
	if err != nil {
		log.Fatalf("Read font: %v", err)
	}

	tt, err := truetype.Parse(b)
	if err != nil {
		log.Fatal(err)
	}
	fontStylized = truetype.NewFace(tt, &truetype.Options{
		Size:    float64(fontStylizedSize) * ebiten.DeviceScaleFactor(),
		DPI:     dpi,
		Hinting: font.HintingFull,
	})

	fontStylizedLarge = truetype.NewFace(tt, &truetype.Options{
		Size:    float64(fontStylizedLargeSize) * ebiten.DeviceScaleFactor(),
		DPI:     dpi,
		Hinting: font.HintingFull,
	})

	// Load images
	imgChalmers, _, err = ebitenutil.NewImageFromFile("/home/trevor/go/src/gitlab.com/tslocum/steamed-hams-but-its-a-graphic-adventure-video-game/assets/img/chalmers.jpg", ebiten.FilterDefault)
	if err != nil {
		log.Fatal(err)
	}

	imgSkinner, _, err = ebitenutil.NewImageFromFile("/home/trevor/go/src/gitlab.com/tslocum/steamed-hams-but-its-a-graphic-adventure-video-game/assets/img/skinner.jpg", ebiten.FilterDefault)
	if err != nil {
		log.Fatal(err)
	}

	sw, _ := imgSkinner.Size()
	imgSkinnerW = float64(sw)

	imgDiningRoom, _, err = ebitenutil.NewImageFromFile("/home/trevor/go/src/gitlab.com/tslocum/steamed-hams-but-its-a-graphic-adventure-video-game/assets/img/diningroom.jpg", ebiten.FilterDefault)
	if err != nil {
		log.Fatal(err)
	}

	ebiten.SetFullscreen(true)
	ebiten.SetWindowTitle("Steamed Hams but it's a graphic adventure video game")

	sw, sh := ebiten.ScreenSizeInFullscreen()
	screenW, screenH = float64(sw), float64(sh)
}

type Game struct{}

func (g *Game) Update(screen *ebiten.Image) error {
	if inpututil.IsMouseButtonJustPressed(ebiten.MouseButtonLeft) {
		x, y := ebiten.CursorPosition()
		testText = fmt.Sprintf("Clicked at %d. %d", x, y)
	}
	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	scale := ebiten.DeviceScaleFactor()

	// Background

	optsDiningRoom := &ebiten.DrawImageOptions{}
	optsDiningRoom.GeoM.Scale(5.5*scale, 5.5*scale)
	optsDiningRoom.GeoM.Translate(0, -300)

	screen.DrawImage(imgDiningRoom, optsDiningRoom)

	// Characters

	optsSkinner := &ebiten.DrawImageOptions{}
	optsSkinner.GeoM.Translate(screenW-imgSkinnerW, 0)

	screen.DrawImage(imgChalmers, nil)
	screen.DrawImage(imgSkinner, optsSkinner)

	printStr := "Superintendent Chalmers"
	txt(screen, printStr, 10, 665)

	printStr = "Principal Skinner"
	skinnerTextSize := (len(printStr) * fontStylizedSize) / 2

	txt(screen, printStr, int(screenW)-int(imgSkinnerW)+((int(imgSkinnerW)-skinnerTextSize)/2)+25, 665)

	printStr = "Ah... Superintendent Chalmers. Welcome."
	txt(screen, printStr, int(screenW/2)-((len(printStr)*fontStylizedSize)/4), int(screenH)-200)

	// TODO print commas as heightened periods
	printStr = testText
	txt(screen, printStr, int(screenW/2)-((len(printStr)*fontStylizedSize)/4), int(screenH)-125)

	ebitenutil.DebugPrint(screen, fmt.Sprintf("FPS: %0.2f", ebiten.CurrentTPS()))
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (int, int) {
	s := ebiten.DeviceScaleFactor()
	return int(float64(outsideWidth) * s), int(float64(outsideHeight) * s)
}

func txt(dst *ebiten.Image, message string, x int, y int) {
	for i := 1; i < 2; i++ {
		text.Draw(dst, message, fontStylized, x+i, y+i, color.Black)
		text.Draw(dst, message, fontStylized, x+i, y-i, color.Black)
		text.Draw(dst, message, fontStylized, x-i, y+i, color.Black)
		text.Draw(dst, message, fontStylized, x-i, y-i, color.Black)
	}

	dropShadow := color.RGBA{
		0,
		0,
		0,
		240,
	}

	text.Draw(dst, message, fontStylized, x-shadowDistance, y+shadowDistance, dropShadow)

	text.Draw(dst, message, fontStylized, x, y, color.White)
}
