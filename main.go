package main

// TODO Determine supported formats and document
import (
	_ "image/jpeg"
	_ "image/png"
	"log"

	"github.com/hajimehoshi/ebiten"
	"gitlab.com/tslocum/steamed-hams-but-its-a-graphic-adventure-video-game/game"
)

func main() {
	game.Initialize()

	if err := ebiten.RunGame(&game.Game{}); err != nil {
		log.Fatal(err)
	}
}
